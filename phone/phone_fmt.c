//phone_fmt.c: Format phone number
//This program gets a phone number from a string and formats it
//Author: Justin Henn
//Date: 4/18/2016
//Assignment: 5
//
//
#include <stdio.h>


void phone_fmt() { 

  char str[50];
  char str2[50];
  
  //get string from user
  
  printf("Please input a phone number: ");
  fgets (str, 50, stdin);

  //checks string for digits and puts those digits in another string

  int x = 0, y = 0, i;
  while (str[x] != '\n') {

    if(isdigit(str[x])) {

      str2[y] = str[x];
      y++;
    }
    x++;
    }

  str2[y] = '\n';


  //print the string

  printf("(");

  for (i = 0; i < 10; i++) {
  
    if (i == 3)
      printf (") ");

    if (i == 6)
      printf ("-");

    printf("%c", str2[i]);
  
  }
  printf("\n");
}


