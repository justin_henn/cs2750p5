//queue.c: Make a queue
//This program is used to make a queue
//Author: Justin Henn
//Date 4/18/2016
//Assignment: 5

#include <stdio.h>
#include <stdlib.h>
#include "queue.h"

/*struct Node {
  
  customer cust;
  struct Node* next;
};*/

/*typedef struct {

  struct Node *front;
  struct Node *rear;
} queue;*/
//struct Node* front = NULL;
//struct Node* rear = NULL;

//creates the queue

queue make () {

  queue q1;

  q1.front = (node *)malloc(sizeof(node));

  //q1.rear = (struct Node *)malloc(sizeof(struct Node));


  q1.front -> next = NULL; //(struct Node *)malloc(sizeof(struct Node));
  q1.rear =  q1.front = NULL;
  q1.numItems = 0;
  return q1;

}

//adds a customer to the queue

queue * enqueue(queue * q1, customer x) {
 
  
  node* temp = (node*)malloc(sizeof(node));
  temp->cust =x; 
  temp->next = NULL;
  if(q1->front== NULL && q1->rear== NULL){
    q1->front = q1->rear = temp;
    q1 ->numItems++;
    return q1;
  }

  q1->rear->next = temp;

  q1->rear = temp;

  q1 -> numItems++;
  return q1;
}

//takes a customer out of queue

customer dequeue(queue * q1) {

  struct Node* temp = q1->front;
  if(q1->numItems == 0) {
    printf("Queue is Empty\n");
    return;
  }

  if(q1->front == q1->rear) {
    q1->front = q1->rear = NULL;
    q1->numItems--;
    return temp->cust;

  }

  else {

    q1->front = q1->front->next;
    q1->numItems--;
    return temp->cust;
  
  }

  free(temp);
}

//see if queue is empty

int total_items(queue * q1) {

  return q1->numItems;

}
