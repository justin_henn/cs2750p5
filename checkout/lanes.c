//lanes.c: run checkout simulation
//This program runs a checkout simulation with one checkout lane and multiple cashiers
//Author: Justin Henn
////Date 4/18/2016
////Assignment: 5

#include <stdio.h>
#include "queue.h"
#include "lanes.h"

//cashier structure

typedef struct Cashier {

  int cust_served;
  int cust_remain;
  int averarge_wait;
  int total_wait;
  int line_service;
  int total_line_service;
  int average_idle;
  int total_idle;
  int next_avail;
  customer current_customer;
} cash;

//function to check who the next cashier should be to take a customer

int next_cashier(cash c1[]) {

   
  int x, y, checker = 0;

  for (x = 0; x < 10; x++) {


      if (c1[x].next_avail <  c1[checker].next_avail) {

	checker = x;
      }

  }


      return checker;
}

//simulation function

void simulate1 (queue q1) {



  int x, difference_times, total_customers = total_items(&q1), served_customers = 0;
  cash all_cashiers[10] = {0};

  int y, cashier_counter = 0;


  int z, idle_total = 0, wait_total = 0, line_total = 0;;


  //loop to process the customers

  for (x = 0; x < 250; x++) {

    
    //take customer out of main queue


      customer temp = dequeue(&q1);

      printf("A customer walks in...\n");
      
      //find next available cashier
      
      cashier_counter = next_cashier(all_cashiers/*, temp*/);

      //find out if the customer had to wait or if cashier was idle

      difference_times = all_cashiers[cashier_counter].next_avail - temp.arrival_time;

      

      //add it to the total wait or total idle depending on outcome

      if (difference_times > 0) {
      
	all_cashiers[cashier_counter].total_wait += difference_times;
	all_cashiers[cashier_counter].next_avail += temp.service_time;
	all_cashiers[cashier_counter].line_service = temp.service_time + difference_times;
	all_cashiers[cashier_counter].total_line_service += all_cashiers[cashier_counter].line_service;
      
      }
      else {

	all_cashiers[cashier_counter].total_idle += (difference_times * -1);
	all_cashiers[cashier_counter].next_avail = temp.arrival_time + temp.service_time;
	all_cashiers[cashier_counter].line_service = temp.service_time;
	all_cashiers[cashier_counter].total_line_service += all_cashiers[cashier_counter].line_service;

      }
      all_cashiers[cashier_counter].current_customer = temp;
      all_cashiers[cashier_counter].cust_served++;

      idle_total = 0;
      wait_total = 0;
      served_customers = 0;

      //loop to print the report every time a customer comes in

   for (z = 0; z < 10; z++) {


    idle_total = all_cashiers[z].total_idle + idle_total;
    wait_total = all_cashiers[z].total_wait + wait_total;
    line_total += all_cashiers[z].line_service;
    served_customers = all_cashiers[z].cust_served + served_customers;

  }

  printf("Average Idle: %7.2f  Average Wait: %7.2f  Average Total Wait: %7.2f  Customers Served: %4d  Customers Remain:  %4d\n", ((float)idle_total/(float)served_customers), ((float)wait_total/(float)served_customers), ((float)line_total/(float)served_customers / 10), served_customers, total_customers - served_customers);
 
      
  
  }

  //report at end of program for all cashiers
  
 printf("\nLane Information:\n\n");



 float average_lane_idle, average_lane_wait, average_total_wait;
 int i;
 for( i = 0 ; i < 10 ; i++ ) {
			           
   average_lane_idle = (float)all_cashiers[i].total_idle / (float)all_cashiers[i].cust_served;
   average_lane_wait = (float)all_cashiers[i].total_wait / (float)all_cashiers[i].cust_served;
   average_total_wait = (float)all_cashiers[i].total_line_service / (float)all_cashiers[i].cust_served;

   printf("Lane %-5d:\tTotal Customers: %-6d\tAverage Wait Time: %7.2f\tAverage Cashier Idle Time: %7.2f\tAverage Total Wait Time: %7.2f\n", (i+1), all_cashiers[i].cust_served, average_lane_wait, average_lane_idle, average_total_wait);
 }

 printf("\n");
 
}
