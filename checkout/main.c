//main.c: runs the main checkout
//This program is the main for the checkout program
//Author: Justin Henn
//Date: 4/18/2016
//Assignment: 5


#include <stdio.h>
#include <ctype.h>
#include "create_cust.h"
#include "queue.h"
#include "lanes.h"
#include "multi_checkout.h"




int main () {
   
  FILE * fd;
  queue que_cust;
  que_cust = make();

  //run the create file program to get the random numbers then open the file
  
  file_create();
  customer cust, cust1;
  if((fd = fopen("customer", "r")) == NULL) {

    printf( "Error opening file\n");
    return ( 1 );
  }


  //read the file in and add to queue

   while ( ! feof ( fd ) ) {
 
     fscanf(fd, "%d", &cust.arrival_time);
     fscanf(fd, "%d", &cust.service_time);

      enqueue(&que_cust, cust);
   }

   int x;
   int what_sim;


   //run first simulation passing the main queue
  printf("Choose a simulation(1 for single lane, 2 for multi lane): ");
  scanf("%d", &what_sim);
  if (what_sim == 1)
   simulate1(que_cust);

   //run second simulation passing the main queue
   
  if(what_sim == 2)
   multi_checkout(que_cust);



  /*for (x = 0; x < 250; x++ ) {
  
  cust1 = dequeue(&que_cust);
  printf("%d, %d\n", cust1.service_time, cust1.arrival_time);

  }*/

  fclose(fd);




return ( 0 );


}

