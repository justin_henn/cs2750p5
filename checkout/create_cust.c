//create_cust.c: Create customer file
//This program creates the customer file with the random numbers
//Author: Justin Henn
//Date: 4/18/2016
//Assignment: 5

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

void file_create () {


  FILE * pFile;

  time_t t;
  srand((unsigned) time(&t));
  int x, serv_time = 0, arriv_time;

  if ( (pFile = fopen ("customer","w+")) == NULL)
  {
    printf ("Cannot open file.\n");

    exit(1);
  }

  //loop to get random numbers and add them to file

  for (x = 0; x < 250; x++) {

    serv_time = (rand() % 121) + serv_time;
    arriv_time = (100 + (rand() % 301));

    fprintf (pFile, "%d %d\n", serv_time, arriv_time);

  }

  fclose(pFile);
}
