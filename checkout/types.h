#ifndef TYPES_H_
#define TYPES_H_

typedef struct
{
          int arrival;
	          int service;
} customerT;

typedef struct listNode
{
          customerT cust;
	          struct listNode *next;
} node;

typedef struct
{
          node *front;
	          node *back;
} queue;

#endif

