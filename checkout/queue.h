//queue.h: H file for queue program
//This is file is necessary to include the queue program
//Author: Justin Henn
//Date 4/18/2016
//Assignment: 5
#ifndef QUEUE_H
#define QUEUE_H


typedef struct
{
    int arrival_time;
      int service_time;

} customer;
  typedef struct Node {     
         customer cust;
	       struct Node* next;
	         } node;

typedef struct {

    node *front;
    node *rear;
    int numItems;
} queue;




queue * enqueue(queue *, customer);
customer dequeue(queue *);
queue make();
int total_items(queue *);

#endif
