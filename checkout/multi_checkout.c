//multi_checkout.c: multi lane checkout simulation
//This program runs a checkout simulation with multiple checkout lanes and cashiers
////Author: Justin Henn
//////Date 4/18/2016
//////Assignment: 5

#include <stdio.h>
#include "queue.h"

typedef struct Cashier {

  int cust_served;
  int cust_remain;
  int averarge_wait;
  int total_wait;
  int line_service;
  int total_line_service;
  int average_idle;
  int total_idle;
  int next_avail;
  customer current_customer;

} cash;

//check if sub queues are empty

int check_all_queues(queue q_arr[]) {

 int checker = 11, x;

  for (x = 0; x < 10; x++) {

    if (total_items(&q_arr[x]) > 0) {

      checker = x;
    }

  }

  return checker;


}


//get the size of all the sub queues
int check_queue_sizes(queue q_arr[]) {

  int checker = 0, x;

  for (x = 0; x < 10; x++) {

    if (total_items(&q_arr[checker]) > total_items(&q_arr[x])) {

      checker = x;
    }

  }

  return checker;

}

void multi_checkout(queue q1) {

  queue queue_array[10];
  customer temp;
  int x, toss = 0, idle_total, wait_total, line_total, served_customers, i;

  for (x = 0; x < 10; x++) {

  queue_array[x] = make();
  }

  cash all_cashiers[10] = {0};
  int count = 0, what_queue, difference_times, total_customers = total_items(&q1) - 1;

  //loop that processes the customers until they are all gone

  while (check_all_queues(queue_array) != 11 || total_items(&q1) > 1)  {

    //checks to see if main queue is empty
      if (total_items(&q1) > 1) {

      /*loop to print the report every time a customer comes in*/
      temp = dequeue(&q1);
      count = temp.arrival_time;
      what_queue = check_queue_sizes(queue_array);
      enqueue(&queue_array[what_queue], temp);
    }

      //checks the lanes to see if customers need processed


    for (x = 0; x < 10; x++ ) {

      idle_total = 0;
      wait_total = 0;
      line_total = 0;
      served_customers = 0;

      //checks to see if a cashier is ready for a new customer

      if (all_cashiers[x].next_avail <= count && count != 0) {

	//checks to see if there is a waiting customer

	if(total_items(&queue_array[x]) > 0) {

	  toss = 1;
	  temp = dequeue(&queue_array[x]);
	  all_cashiers[x].current_customer = temp;
	  all_cashiers[x].cust_served++;
	  difference_times = all_cashiers[x].next_avail - temp.arrival_time;

	  //process customer

	  if (difference_times > 0) {

	    all_cashiers[x].total_wait += difference_times;
	    all_cashiers[x].next_avail += temp.service_time;
	    all_cashiers[x].line_service = temp.service_time + difference_times;
	    all_cashiers[x].total_line_service += all_cashiers[x].line_service;
   
	  }

	  else {

	    all_cashiers[x].total_idle += (difference_times * -1);
	    all_cashiers[x].next_avail = temp.arrival_time + temp.service_time;
	    all_cashiers[x].line_service = temp.service_time;
	    all_cashiers[x].total_line_service += all_cashiers[x].line_service;
	  }
	}
      }
 // printf("Average Idle: %7.2f  Average Wait: %7.2f  Average Total Wait: %7.2f  Customers Served: %4d  Customers Remain:  %4d\n", ((float)all_cashiers[x].total_idle/(float)all_cashiers[x].cust_served), ((float)all_cashiers[x].total_wait/(float)all_cashiers[x].cust_served), ((float)all_cashiers[x].total_line_service/(float)all_cashiers[x].cust_served),served_customers, total_customers - served_customers);
   
    }

    //print reporting

    if (toss == 1) {

      for (x = 0; x < 10; x++ ) {
	idle_total = all_cashiers[x].total_idle + idle_total;
 	wait_total = all_cashiers[x].total_wait + wait_total;
	line_total = all_cashiers[x].total_line_service + line_total;;
 	served_customers = all_cashiers[x].cust_served + served_customers;

      }

     // printf("Average Idle: %7.2f  Average Wait: %7.2f  Customers Served: %4d  Customers Remain:  %4d\n", ((float)idle_total/(float)served_customers), ((float)wait_total/(float)served_customers), served_customers, total_customers - served_customers);
      printf("A customer walks in...\n");

      printf("Average Idle: %7.2f  Average Wait: %7.2f  Average Total Wait: %7.2f  Customers Served: %4d  Customers Remain:  %4d\n", ((float)idle_total/(float)served_customers), ((float)wait_total/(float)served_customers), ((float)line_total/(float)served_customers ),served_customers, total_customers - served_customers);
    } 
    count ++;
    toss = 0;
  }

  //print end report
printf("\nLane Information:\n\n");



 float average_lane_idle, average_lane_wait, average_total_wait;
 for( i = 0 ; i < 10 ; i++ ) {
			           
   if( all_cashiers[i].cust_served == 0 ) {
   	average_lane_idle = 0;
	average_lane_wait = 0;
	average_total_wait = 0;
   }
   else {
  	average_lane_idle = (float)all_cashiers[i].total_idle / (float)all_cashiers[i].cust_served;
   	average_lane_wait = (float)all_cashiers[i].total_wait / (float)all_cashiers[i].cust_served;
  	average_total_wait = (float)all_cashiers[i].total_line_service / (float)all_cashiers[i].cust_served;
   }
   printf("Lane %-5d:\tTotal Customers: %-6d\tAverage Wait Time: %7.2f\tAverage Cashier Idle Time: %7.2f\tAverage Total Wait Time: %7.2f\n", (i+1), all_cashiers[i].cust_served, average_lane_wait, average_lane_idle, average_total_wait);
 }

 printf("\n");

}

