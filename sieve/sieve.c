//sieve.c: Find Primes
//This program finds primes by using the sieve technique
//Author: Justin Henn
//Date 4/18/2016
//Assignment: 5


#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <math.h>

//function that does the sieve process of checking to see if a number is prime

void check_value(unsigned char * a) {

  int max_int = 100000, stop_point = 316, p = 2, multiple, num_check, iter1, iter2;
  
//loop that stops when p is less than 316 which is square root of 100000 and goes through all the numbers up to 100000 to see what is prime

  while (p < stop_point) {

    multiple = 2;
    num_check = p * multiple;

    //loop that makes sure the number to check does not go over 100000 and also checks to see if the number is set to one and does if it is not one

    while (num_check < max_int) {

    iter1 = num_check / 8;
    iter2 = num_check % 8;

    if ((a[iter1] & (1 << iter2)) != 0) {

      multiple++;
      num_check = p * multiple;
      continue;
    }
    else
      a[iter1] |= 1 << iter2;
   
    multiple++;
    num_check = p * multiple;
    }

    p++;
  }
}

//function that prints the prime numbers

void print_it(unsigned char * a) {

  int z, num = 0, iter;
  for ( iter = 0; iter < 12500; iter++ ) {

    for ( z = 0; z < 8; z++, num++ ) {

      if ((a[iter] & (1 << z)) != 0) {

	continue;
      }
      else
	printf( "%d\n", num);
    }
  }
}

//function that calls the function to run the program

int main() {
  
  unsigned char checker[12500];
  memset(checker, 0, sizeof(checker));
 
  check_value(checker);
  print_it(checker);

}

